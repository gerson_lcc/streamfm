//
//  TopTracksViewController.swift
//  StreamFM
//
//  Created by Gerson Costa on 20/03/2019.
//  Copyright © 2019 Gerson Costa. All rights reserved.
//

import UIKit
import AVKit
import GoogleMobileAds

class TopTracksViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var bannerView: GADBannerView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private let apiKey = "dd4a4c97b7b10591188050ed778c52e8"
    private var tracks: [(name: String, url: String, image: String)]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activityIndicator.startAnimating()
        setupBanner()
        
        if view.frame.width > 320 {
            collectionView.contentInset = UIEdgeInsets(top: 20, left: 15, bottom: 20, right: 15)
        }

        collectionView.delegate = self
        collectionView.dataSource = self
        
        parse()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWebview" {
            let vc = segue.destination as! WebViewController
            let trackURL = "https://www.last.fm/music/Portishead/_/Cowboys"
            vc.url = trackURL
        }
    }
    
    func setupBanner() {
        bannerView.rootViewController = self
        bannerView.adUnitID = "ca-app-pub-5882407912847877/2481432498"
        bannerView.load(GADRequest())
    }
    
    func parse() {
        let tracksParser = TopTrackManager()
        
        tracksParser.parseTopTracks(originURL: "https://ws.audioscrobbler.com/2.0/?method=user.gettoptracks&user=rj&api_key=\(apiKey)") { (tracks: [(name: String, url: String, image: String)]) in
            
            self.tracks = tracks
            OperationQueue.main.addOperation {
                self.collectionView.reloadData()
                self.activityIndicator.stopAnimating()
            }
        }
    }

}

extension TopTracksViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let tracks = tracks else { return }
        
        guard let vc = UIStoryboard.init(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "WebVC") as? WebViewController else { return }
        vc.url = tracks[indexPath.row].url
        
        navigationController?.pushViewController(vc, animated: true)
    }
}

extension TopTracksViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let tracks = tracks else {
            return 0
        }
        
        return tracks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TrackCell", for: indexPath) as! TopTrackCollectionViewCell
        
        guard let track = tracks?[indexPath.row] else {
            return UICollectionViewCell()
        }
        
        cell.configureCellWith(name: track.name, songURL: track.url, imageURL: track.image)
        
        return cell
    }
}
