//
//  WebViewController.swift
//  StreamFM
//
//  Created by Gerson Costa on 20/03/2019.
//  Copyright © 2019 Gerson Costa. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: UIViewController {

    @IBOutlet weak var webview: WKWebView!
    
    var url: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadSongPage()
    }

    func loadSongPage() {
        guard let songURL = URL(string: url) else { return }
        let request = URLRequest(url: songURL)
        webview.load(request)
    }
    
}
