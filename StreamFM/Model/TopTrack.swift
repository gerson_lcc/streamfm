//
//  TopTrack.swift
//  StreamFM
//
//  Created by Gerson Costa on 20/03/2019.
//  Copyright © 2019 Gerson Costa. All rights reserved.
//

import Foundation

class Toptrack {
    
    private(set) var trackName: String
    private(set) var trackImageURL: String
    private(set) var trackURL: String
    
    init(name: String, imageURL: String, songURL: String) {
        trackName = name
        trackImageURL = imageURL
        trackURL = songURL
    }
}
