//
//  TopTrackManager.swift
//  StreamFM
//
//  Created by Gerson Costa on 20/03/2019.
//  Copyright © 2019 Gerson Costa. All rights reserved.
//

import Foundation

class TopTrackManager: NSObject {
    
    fileprivate var tracks: [(name: String, url: String, image: String)] = []
    fileprivate var currentElement = ""
    
    fileprivate var currentName: String = "" {
        didSet {
            currentName = currentName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    fileprivate var currentURL: String = "" {
        didSet {
            currentURL = currentURL.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    fileprivate var currentImage: String = "" {
        didSet {
            currentImage = currentImage.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        }
    }
    
    fileprivate var topTrackCompletionHandler: (([(name: String, url: String, image: String)]) -> Void)?
    
    func parseTopTracks(originURL: String, completionHandler: (([(name: String, url: String, image: String)]) -> Void)?) -> Void {
        
        self.topTrackCompletionHandler = completionHandler
        
        let request = URLRequest(url: URL(string: originURL)!)
        let task = URLSession.shared.dataTask(with: request) { (data, response, error) in
            
            guard let data = data else {
                if let error = error {
                    print(error)
                }
                return
            }
            
            let parser = XMLParser(data: data)
            parser.delegate = self
            parser.parse()
        }
        task.resume()
    }
}

extension TopTrackManager: XMLParserDelegate {
    func parserDidStartDocument(_ parser: XMLParser) {
        tracks = []
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String] = [:]) {
        
        currentElement = elementName
        
        if currentElement == "track" {
            currentName = ""
            currentURL = ""
            currentImage = ""
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        
        switch currentElement {
            case "name":
                currentName += string
            case "url":
                if string.contains("http") && currentURL.count == 0 {
                    currentURL += string
                }
            case "image":
                if string.contains("/174s/") {
                    currentImage += string
                }
            default:
                break
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        if elementName == "track" {
            let track = (name: currentName, url: currentURL, image: currentImage)
//            print(track.url)
//            print("========================")
            tracks += [track]
        }
    }
    
    func parserDidEndDocument(_ parser: XMLParser) {
        topTrackCompletionHandler?(tracks)
    }
    
    func parser(_ parser: XMLParser, parseErrorOccurred parseError: Error) {
        print(parseError.localizedDescription)
    }
}
