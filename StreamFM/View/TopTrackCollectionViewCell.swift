//
//  TopTrackCollectionViewCell.swift
//  StreamFM
//
//  Created by Gerson Costa on 20/03/2019.
//  Copyright © 2019 Gerson Costa. All rights reserved.
//

import UIKit

class TopTrackCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var trackNameLabel: UILabel!
    @IBOutlet weak var trackImage: UIImageView!
    @IBOutlet weak var artistLabel: UILabel!
    
    func configureCellWith(name: String, songURL: String, imageURL: String) {
        let artistAndTrack = songURL.components(separatedBy: "/music/")
        let artist = artistAndTrack[1].components(separatedBy: "/_/")[0].replacingOccurrences(of: "+", with: " ")
        let track = artistAndTrack[1].components(separatedBy: "/_/")[1].replacingOccurrences(of: "+", with: " ")
        
        artistLabel.text = artist
        trackNameLabel.text = track
        trackImage.layer.cornerRadius = trackImage.frame.width / 2
        
        getImage(fromURL: imageURL)
    }
    
    func getImage(fromURL url: String) {
        guard let imageURL = URL(string: url) else {
            print("Invalid image url")
            trackImage.image = UIImage(named: "error")
            return
        }
        
        let task = URLSession.shared.dataTask(with: URLRequest(url: imageURL)) { (data, response, error) in
            
            guard let imageData = data else {
                if let error = error {
                    print(error)
                }
                return
            }
            
            DispatchQueue.main.async {
                self.trackImage.image = UIImage(data: imageData)
            }
        }
        task.resume()
        
    }
}
